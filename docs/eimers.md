---
search: false
navbar: false
---

[Zur Übersicht](/)

# Thomas Eimers

![Thomas Eimers](./t-eimers.jpg)

## Kinder

- Livia (Ü3 Gruppe)
- Lois (Seepferdchen)

## Sprachen
- Deutsch
- Englisch

## Kontakt
- Über die WhatApp Elterngruppe & 0177 3025152
