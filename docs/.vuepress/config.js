module.exports = {
    title: 'Elternbeirat 2023/24',
    description: 'Elternbeirat des FRÖBEL-Kindergarten & Familienzentrums Kindervilla in Essen',
    base: '/elternbeirat/',
    dest: 'public'
}
