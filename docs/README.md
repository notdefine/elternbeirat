---
home: true
search: false
navbar: false
heroImage: /qr-code.svg

features:
- title: Pia Schuster (Vorsitzende)
  details: Vorsitzende des Elternbeirats und im fünften Jahr Teil des Elternbeirats.
- title: Faten Omayrat
  details: Als vierfache Mutter bin ich mittlerweile im zehnten Jahr in der Einrichtung.
- title: Thomas Eimers
  details: Zum dritten Mal Teil des Elternbeirats und Vater von Livia und Lois.
- title: Mike Piel
  details: Vater von Leni und Merle.
- title: Miriam Foltmer (VertreterIn)
  details: Seit 2018 Teil der Kita-Eltern und zum dritten mal Teil des Elternbeirats.
- title: Frau Berg (VertreterIn)
  details:
- title: Frau Müller (Vertreterin)
  details: 
- title: Frau Bernsmann (Vertreterin)
  details: 

footer: Stand 30.08.2023
---

::: tip
Es gibt eine WhatsApp-Gruppe für die Eltern der Kita. Diese dient zum Kennenlernen und zum Austausch.
Jeder kann dieser Gruppe beitreten, indem er/sie den QR-Code im Eingangsbereich der Kindervilla scannt.
:::


## Kontakt des Elternbeirats

* [Pia Schuster](schuster.md)
* [Faten Omayrat](omayrat.md)
* [Thomas Eimers](eimers.md)
* [Maik Piel](piel.md)
* [Miriam Foltmer](foltmer.md)
